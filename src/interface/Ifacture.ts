export interface  Ifacture{
    id?: string;
    titre?: string;
    description?: string;
    prix?: string;
    date?: Date;
    id_note_frais?: string;
    categorie?:string;
    refacturable?: boolean;
    fournisseur?: string;
    tva?: string;
    path?: string;
}