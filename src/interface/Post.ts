export interface Post{
    html_attributions: string;
    next_page_token: string;
    results: [
        {name: string;
        }
    ]
}