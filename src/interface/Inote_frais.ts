export interface Inote_frais{
    titre?: string;
    montant?: number;
    id?: string;
    id_depenses?: string[];
    date?: Date;
    status?: Status;
    paths? : string[];

} 


export enum Status {
    en_cours = 'en cours',
    verouille = 'verouillé'
}