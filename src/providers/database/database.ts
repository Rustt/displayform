import { Injectable } from '@angular/core';
import {SQLite, SQLiteObject} from '@ionic-native/sqlite';
import { Ifacture } from '../../interface/Ifacture';
import { Inote_frais, Status } from '../../interface/Inote_frais';

@Injectable()
export class Database {
 

  private isOpen: boolean;
  private storage: SQLiteObject;

  requetes =[
    'SELECT id FROM depenses WHERE depenses.id_note_frais = id_note',
    'SELECT id FROM note_frais WHERE status = Status.en_cours',

  ];

  constructor(private db: SQLite) {
    this.initialiseDB();
    console.log('hello database');

    }
    

  openDatabase(){
    this.db.create({
      name: 'jePilote.db',
      location: 'default'
    })
  }
  

  initialiseDB() {
    this.db.create({
      name: 'jePilote.db',
      location: 'default' 
    }).then((execute: SQLiteObject) => {
      this.storage = execute;
      this.createTables();
    })
    .catch(e => console.log(e));
  }


  createTables() {
    this.storage.executeSql("CREATE TABLE IF NOT EXISTS depenses(id INTEGER PRIMARY KEY AUTOINCREMENT, titre TEXT, prix NUMBER, description TEXT, date DATETIME, path TEXT,  id_note_frais TEXT, categorie TEXT)", {})
      .then(res => console.log('Requête executée!'));
      this.storage.executeSql("CREATE TABLE IF NOT EXISTS notes_frais (id INTEGER PRIMARY KEY AUTOINCREMENT, titre TEXT, status STATUS, montant NUMBER, description TEXT, date DATETIME DEFAULT CURRENT_TIMESTAMP)", {})
      .then(res => console.log('Requête executée!'));
    
  }
//renvoi l'id de la note de frais en cours
  checkedNoteEnCours(){
    let status = Status.en_cours;
    return new Promise((resolve, reject) => {
     this.db.create({
      name: 'jePilote.db',
      location: 'default'
    }).then((execute: SQLiteObject) => {
    execute.executeSql('SELECT * FROM notes_frais WHERE status = ?', [status]).then( response => {
      //console.log(response);
      let entries =[];
      for( let index = 0; index < response.rows.length; index ++){
        entries.push({
          id: response.rows.item(index).id,
          titre: response.rows.item(index).titre,
          montant: response.rows.item(index).montant,
          date: response.rows.item(index).date
      })
    }
    if(entries[0] == undefined){
      console.log("Pas de Note en Cours");
      resolve(null); // !==undefined
     
    }else {

      resolve(entries[0]);
    }
  }, (error) =>{
    reject(error);
  
    });
    }).catch(e => console.log(e));

  }
);
}

getIDcheckedNoteEnCours(){
  let status = Status.en_cours, Idfound;
  return new Promise((resolve, reject) => {
   this.db.create({
    name: 'jePilote.db',
    location: 'default'
  }).then((execute: SQLiteObject) => {
  execute.executeSql('SELECT * FROM notes_frais WHERE status = ?', [status]).then( response => {
    //console.log(response);
    let entries =[];
    for( let index = 0; index < response.rows.length; index ++){
      entries.push({
        id: response.rows.item(index).id,
        titre: response.rows.item(index).titre,
        montant: response.rows.item(index).montant
    })
  }
  if(entries[0] == undefined){
    resolve(null); // !==undefined
   
  }else {
    Idfound = entries[0].id;
   // console.log(entries[0].id+ 'SDFGHJK');
    console.log(Idfound);
    resolve(Idfound);
  }
}, (error) =>{
  reject(error);

  });
  }).catch(e => console.log(e));

}
);
}


// ajoute la dépense avec l'id de la note de frais en cours
    addDepense(depense: Ifacture){
      let note_en_cours, Id: string;
      return new Promise((resolve, reject) => {
        this.db.create({
          name: 'jePilote.db',
          location: 'default'
        }).then((execute: SQLiteObject) => {
          this.checkedNoteEnCours()
          .then((data) => {
            console.log(data +'HUUUU');
            if (data !== null){            
              note_en_cours = <Inote_frais> data;
            Id = <string> note_en_cours.id;
              console.log(note_en_cours);
              console.log(depense);
              console.log(depense.prix);
              console.log(depense.categorie);

              console.log(note_en_cours.id);

              //Id =  <string>note_en_cours.id;
              //depense.id_note_frais = Id;
             // Id = <string> note_en_cours
              execute.executeSql('INSERT INTO depenses VALUES(NULL,?,?,?,?,?,?,?)', [depense.titre, depense.prix, depense.description,depense.date ,depense.path, Id,depense.categorie]) //subscribe ?
              .then(data => console.log('Depense ajoutee ' +JSON.stringify(data)))
            }
          
            resolve(Id);
          }, (error) =>{
            reject(error);
          
            });
            }).catch(e => console.log(e));
          })

          }



    updateNoteFrais(depense: Ifacture, Idnote: any){
      let price: number;
      return new Promise((resolve, reject) => {
        this.db.create({
          name: 'jePilote.db',
          location: 'default'
        }).then((execute: SQLiteObject) => {
          this.getNote(Idnote)
            .then((note: Inote_frais) => {
              //console.log(note.montant + 'note a ajouter');
              console.log(note);
              let newnote = <Ifacture> depense;
              let newmontant;
              console.log(depense.prix);
              price= parseFloat(depense.prix);

              console.log(note.montant + 'PRICE2');

              if(note.montant == null){
                newmontant = price;
                console.log(newmontant);

              }else{
               newmontant = note.montant + price;
              }
              console.log('NEWMONTANT :' + newmontant);

              execute.executeSql('UPDATE notes_frais SET montant=? WHERE id=?', [newmontant, Idnote])
                .then((data) => { 
                  console.log('Montant mis à jour');
                resolve(data);
          }, (error) =>{
            reject(error);
                           });
 });
          }).catch(e => console.log(e));
      })
    }
        
    
    
getNote(id: string) {
  return new Promise((resolve, reject) => {
    this.db.create({
      name: 'jePilote.db',
      location: 'default'
    }).then((execute: SQLiteObject) => {
    execute.executeSql('SELECT * FROM notes_frais WHERE id =?', [id])
      .then(response => {
          let entries: Inote_frais;
              entries = response.rows.item(0);
          resolve(entries);
      }, (error) =>{
        reject(error);
      
        })
    })
  })
}

getIdDepensesFromNote(note : Inote_frais){

  
}






/* updateDepense(depense: Ifacture) {
  let sql = 'UPDATE depenses SET  prix=?, description=?, date=? WHERE titre=?';

  this.openDatabase().then((execute: SQLiteObject) => {
  return execute.executeSql(sql, [depense.titre, depense.prix, depense.description, depense.date, depense.path]);
  })
} */

    
  




  

getDepenses(){
    return new Promise((resolve, reject) => {
      console.log('cc database');
    this.db.create({
      name: 'jePilote.db',
      location: 'default'
    }).then((execute: SQLiteObject) => {
    execute.executeSql('SELECT * FROM depenses', []).then( response => {
      let entries =[];
      for( let index = 0; index < response.rows.length; index ++){
        entries.push({

          titre : response.rows.item(index).titre,
          prix: response.rows.item(index).prix,
          id: response.rows.item(index).id,
          id_note_frais: response.rows.item(index).id_note_frais,
          description: response.rows.item(index).description,
          path: response.rows.item(index).path,  
          date: response.rows.item(index).date,
          categorie: response.rows.item(index).categorie

      })
    }
    resolve(entries);
  }, (error) =>{
    reject(error);
  
    });
    }).catch(e => console.log(e));

  });
}


getNotesFrais(){
  return new Promise((resolve, reject) => {
    console.log('cc database');
  this.db.create({
    name: 'jePilote.db',
    location: 'default'
  }).then((execute: SQLiteObject) => {
  execute.executeSql('SELECT * FROM notes_frais', []).then( response => {
    let entries =[];
    for( let index = 0; index < response.rows.length; index ++){
      entries.push({

        titre : response.rows.item(index).titre,
        montant: response.rows.item(index).montant,
        id: response.rows.item(index).id,
        status: response.rows.item(index).status,
        date: response.rows.item(index).date

    })
  }
  resolve(entries);
}, (error) =>{
  reject(error);

  });
  }).catch(e => console.log(e));

});
}



changeStatusNote(note: Inote_frais){

}

  getDepensesFromNote(note: Inote_frais){
    return new Promise((resolve, reject) => {
    this.db.create({
      name: 'jePilote.db',
      location: 'default'
    }).then((execute: SQLiteObject) => {
    execute.executeSql('SELECT * FROM depenses WHERE id_note_frais=?', [note.id]).then( response => {
      let entries = [];
      for( let index = 0; index < response.rows.length; index ++){
        entries.push({

          titre : response.rows.item(index).titre,
          prix: response.rows.item(index).prix,
          id: response.rows.item(index).id,
          id_note_frais: response.rows.item(index).id_note_frais,
          description: response.rows.item(index).description,
          path: response.rows.item(index).path,  
          date: response.rows.item(index).date,
          categorie: response.rows.item(index).categorie


      })
    }
    resolve(entries);
  }, (error) =>{
    reject(error);
  
    });
    }).catch(e => console.log(e));

  });

  }


  //boucler sur cette fonction depuis depenses[]
  addNoteFrais(depense: Ifacture) {
    status = Status.en_cours;
    return new Promise((resolve, reject) => {
    this.db.create({
      name: 'jePilote.db',
      location: 'default'
    }).then((execute: SQLiteObject) => {
    execute.executeSql('INSERT INTO notes_frais VALUES(NULL,?,?,?,?,?)',['Note en cours',depense.prix , , ,status ]) //subscribe ?
    .then((data) => { console.log('Note de frais ajoutée'+JSON.stringify(data))
        
      resolve(data);
      }, (error) =>{
      reject(error);
      })

      }).catch(err =>console.log(err))
    })
  }
  

  deleteAllDepenses() {
    this.db.create({
      name: 'jePilote.db',
      location: 'default'
    }).then((execute: SQLiteObject) => {
      execute.executeSql('DELETE FROM depenses', [])
      .then(res => {
        console.log(res);
       
      })
      .catch(e => console.log(e));
    }).catch(e => console.log(e));
  }



deleteAllNoteFrais() {
  this.db.create({
    name: 'jePilote.db',
    location: 'default'
  }).then((execute: SQLiteObject) => {
    execute.executeSql('DELETE FROM notes_frais', [])
    .then(res => {
      console.log(res);
     
    })
    .catch(e => console.log(e));
  }).catch(e => console.log(e));
}
deleteTables() {
  this.db.create({
    name: 'jePilote.db',
    location: 'default'
  }).then((execute: SQLiteObject) => {
    execute.executeSql('DROP TABLE depenses', [])
    .then(res => {
      console.log(res);
     
    })
    .catch(e => console.log(e));
  }).catch(e => console.log(e));
}


Sort(){

}



}




//https://www.djamware.com/post/59c53a1280aca768e4d2b143/ionic-3-angular-4-and-sqlite-crud-offline-mobile-app








