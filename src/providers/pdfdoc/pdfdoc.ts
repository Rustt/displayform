import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;


/*
  Generated class for the PdfdocProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PdfdocProvider {

  constructor() {
    console.log('Hello PdfdocProvider Provider');
  }

    doc = null;
    document = null;
    ownerTableBody = [
      [
          { text: 'Nom'.toUpperCase(), style: 'tableHeader' },
          { text: 'Prénom'.toUpperCase(), style: 'tableHeader' },
          { text: 'Email'.toUpperCase(), style: 'tableHeader' }
      ],
      [
          {text: 'Comet', style: 'tableCell'},
          {text: 'Martin', style: 'tableCell'},
          {text: 'myemail@email', style: 'tableCell'}
      ]
  ];

  tabFrais = [
      [
          { text: 'Date'.toUpperCase(), style: 'tableHeader' },
          { text: 'Catégorie'.toUpperCase(), style: 'tableHeader' },
          { text: 'Montant TTC'.toUpperCase(), style: 'tableHeader' },
          { text: 'Montant HT'.toUpperCase(), style: 'tableHeader' },
          { text: 'Montant TVA'.toUpperCase(), style: 'tableHeader' }
      ]
  ];
    
    clear = function() {
        this.doc = null;
        this.document = null;
    }

    lgColor = '#c4c4c4';
    hdColor = '#ededed';
    tbLayout = {
            hLineWidth: function(i, node) {
                return i === 0 ? 0 : 1;
            },
            vLineWidth: function(i, node) {
                return 1;
            },
            hLineColor: function(i, node) {
                return i === 0 ? 'white' : this.lgColor;
            },
            vLineColor: function(i, node) {
                if (node.line === undefined) {
                    node.line = 0;
                }
                if (i === node.table.widths.length) {
                    node.line++;
                }
                if (node.line === 0) {
                    return this.hdColor;
                }
                if (node.line === node.table.body.length && i === node.table.widths.length) {
                    node.line = 0;
                }
                return (i === 0 || i === node.table.widths.length) ? 'white' : this.lgColor;
            }
        };

        titleStyle = {
            fontSize: 14,
            bold: true,
            margin: [0, 0, 0, 10],
            alignment: 'center'
        };

        titleLeftStyle = {
            fontSize: 12,
            bold: true,
            margin: [0, 20, 0, 10],
            alignment: 'left'
        };


    formpdf(){

        for (var i = 0; i < this.tabFrais.length; i++) {
            var row = [];
            row.push({
                text: '12/12/2012',//$scope.getDateJMAFromString($scope.tabFrais[i].date),
                style: 'tableCell'
            });
            row.push({
                text: this.tabFrais[i],
                style: 'tableCell'
            });
            row.push({
                text: '€' + this.tabFrais[i],
                style: 'tableCell'
            });

/*             row.push({
                text: '€' + $scope.tabFrais[i].ht,
                style: 'tableCell'
            });
            row.push({
                text: '€' + $scope.tabFrais[i].tva,
                style: 'tableCell'
            }); */
            this.tabFrais.push(row);
        }
        this.doc = {
            info: {
                title: 'Note de frais',
                author: 'JePilote Mes Factures'
            },
            pageSize: 'A4',
            pageMargins: [ 40, 40, 40, 20 ],
            content: [
                {
                    text: 'Note de frais',
                    style: 'title'
                },
                {
                    style: 'table',
                    table: {
                        headerRows: 1,
                        widths: [ '20%', '20%', '60%' ],
                        body: this.ownerTableBody
                    },
                    layout: this.tbLayout
                },
                {
                    table: {
                        headerRows: 1,
                        widths: [ '30%', '30%' ],
                        body: [
                            [{ text: 'DU', style: 'tableHeader' }, { text: 'AU', style: 'tableHeader' }],
                            //[{text: $scope.getDateJMAFromString($scope.periodeNdfDu), style: 'tableCell'}, {text: $scope.getDateJMAFromString($scope.periodeNdfAu), style: 'tableCell'}]
                        ]
                    },
                    layout: this.tbLayout
                },
                {
                    text: 'Tableau récapitulatif des frais',
                    style: 'titleLeft'
                },
                {
                    style: 'table',
                    table: {
                        headerRows: 1,
                        widths: [ '20%', '35%', '15%', '15%', '15%' ],
                        body: this.tabFrais
                    },
                    layout: this.tbLayout
                },
                {
                    table: {
                        headerRows: 1,
                        widths: [ '20%', '20%', '20%' ],
                        body: [
                            [
                                { text: 'TOTAL TTC', style: 'tableHeader' },
                                { text: 'TOTAL HT', style: 'tableHeader' },
                                { text: 'TOTAL TVA', style: 'tableHeader' }
                            ] //,
                   /*          [
                                {text: '€' + $scope.ndfTotal.ttc, style: 'tableCell'},
                                {text: '€' + $scope.ndfTotal.ht, style: 'tableCell'},
                                {text: '€' + $scope.ndfTotal.tva, style: 'tableCell'}
                            ] */
                        ]
                    },
                    layout: this.tbLayout
                }
            ],
               styles: {
                title: this.titleStyle,
                titleLeft: this.titleLeftStyle,
                tableCell: {
                    fontSize: 10,
                    margin: 2
                },
                tableHeader: {
                    fontSize: 9,
                    color: this.lgColor,
                    margin: 2,
                    fillColor: this.hdColor
                },
                table: {
                    margin: [0, 0, 0, 10]
                }
            } 
        };/*
           if ($scope.tabIK.length) {
            this.doc.content.push({
                text: 'Tableau récapitulatif des frais kilométriques',
                style: 'titleLeft'
            });

            var tabIk = [
                [
                    { text: 'Date'.toUpperCase(), style: 'tableHeader' },
                    { text: 'Catégorie'.toUpperCase(), style: 'tableHeader' },
                    { text: 'Nombre de Km'.toUpperCase(), style: 'tableHeader' },
                    { text: 'Véhicule'.toUpperCase(), style: 'tableHeader' },
                    { text: 'Montant indemnisation'.toUpperCase(), style: 'tableHeader' }
                ]
            ];

            for (var i = 0; i < $scope.tabIK.length; i++) {
                var row = [];
                row.push({
                    text: $scope.getDateJMAFromString($scope.tabIK[i].date),
                    style: 'tableCell'
                });
                row.push({
                    text: $scope.tabIK[i].categorie,
                    style: 'tableCell'
                });
                row.push({
                    text: $scope.tabIK[i].nombre_km + '',
                    style: 'tableCell'
                });
                row.push({
                    text: $scope.tabIK[i].ik_vehicule,
                    style: 'tableCell'
                });
                row.push({
                    text: '€' + $scope.tabIK[i].ik_montant,
                    style: 'tableCell'
                });
                tabIk.push(row);
            }

            this.doc.content.push({
                style: 'table',
                table: {
                    headerRows: 1,
                    widths: [ '15%', '30%', '10%', '25%', '20%' ],
                    body: tabIk
                },
                layout: tbLayout
            });

            this.doc.content.push({
                table: {
                    headerRows: 1,
                    widths: [ '40%' ],
                    body: [
                        [
                            {text: 'Total indemnisation kilométrique'.toUpperCase(), style: 'tableHeader' }
                        ],
                        [
                            {text: '€' + $scope.totalIK, style: 'tableCell'}
                        ]
                    ]
                },
                layout: tbLayout
            });
        } 
       
        this.doc.content.push({
            text: 'Totaux par poste de dépense',
            style: 'titleLeft'
        });

        var tabTotal = [
            [
                { text: 'Catégorie'.toUpperCase(), style: 'tableHeader' },
                { text: 'TOTAL TTC', style: 'tableHeader' }
            ]
        ];

        for (var i = 0; i < $scope.totalCategory.length; i++) {
            var row = [];
            row.push({
                text: $scope.totalCategory[i].categorie,
                style: 'tableCell'
            });
            row.push({
                text: '€' + $scope.totalCategory[i].total,
                style: 'tableCell'
            });
            tabTotal.push(row);
        }

        this.doc.content.push({
            table: {
                headerRows: 1,
                widths: [ '40%', '20%' ],
                body: tabTotal
            },
            layout: tbLayout
        });
    }

    this.getBlob = function(callback) {
        this.buildDocument();
        this.document.getBuffer(function(buffer) {
            var currentBlob = new Blob([buffer.toArrayBuffer()], { type : "application/pdf" });
            callback(URL.createObjectURL(currentBlob));
        });
    }

    this.openBase64 = function() {
        this.buildDocument();
        this.document.open();
    }

    this.getData = function(callback) {
        this.buildDocument();
        this.document.getBuffer(function(result) {
            var blob;
            try {
                blob = new Blob([result.toArrayBuffer()], { type: 'application/pdf' });
            }
            catch (e) {
                // Old browser which can't handle it without making it an byte array (ie10)
                if (e.name == "InvalidStateError") {
                    console.log('oldBrowsers', e)
                    var byteArray = new Uint8Array(result.toArrayBuffer());
                    blob = new Blob([byteArray.buffer], { type: 'application/pdf' });
                }
            }
//            console.log(blob);return true;
            callback(blob);
        });
    }

    this.buildDocument = function() {
//        console.log(this.doc);
        if (!this.document) {
            this.document = pdfMake.createPdf(this.doc);
        }
    }


    this.isReady = function() {
        return this.doc !== null;
    }



  

  }



*/
    }}