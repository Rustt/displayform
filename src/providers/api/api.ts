import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import { Post } from '../../interface/Post';
import { Response } from '@angular/http';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {

  private url: string = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=44.840024,%20%20-0.586275&radius=500&type=restaurant&key=%20AIzaSyB9IwISvShHQqIIeeczKWvZzWJXp2FVoFo'
  constructor(public http: Http) {
    console.log('Hello ApiProvider Provider');
  }


			//.catch((error: HttpErrorResponse) => this.handleAngularJsonBug(error))

      getResto(): Observable<Post[]>{
        return this.http
          .get(this.url)
          .map((response: Response)=>{
            return <Post[]>response.json().results;
          });
        }

}
