import { Injectable } from '@angular/core';
import { Storage } from "@ionic/storage";
import { Ifacture } from '../../interface/Ifacture';
import { NotesfraisProvider } from '../../providers/notesfrais/notesfrais';
import { Inote_frais } from '../../interface/Inote_frais';


// provider stockage mémoire interne notes + demande création notes de frais associée

const STORAGE_KEY = 'notes';

@Injectable()
export class MynotesProvider {

/*   notes: Ifacture[] = [
    {titre: 'Restaurant La plume',
    description: 'RDV lobby',
    prix: 68.4,
    id: '1',
    id_note_frais: '1',
    path: 'assets/imgs/couvert.png'
    },
    {titre: 'Péage',
    description: 'Paris - Orléans',
    prix: 45.8,
    id: '2',
    id_note_frais: '1',

    path: 'assets/imgs/IK.png'
    },
    {titre: 'Frais Auchan',
    description: 'Pot convivial',
    prix: 123.09,
    id: '3',
    id_note_frais: '2',
    path: 'assets/imgs/autre.png'
    }
  ]; */

  constructor(private storage: Storage,
              private notes_frais: NotesfraisProvider ) {
    console.log('Hello MynotesProvider Provider');
  }

  getNoteKey(){
    return STORAGE_KEY + this.getRandomInt().toString();
  }

  getRandomInt() {
 
    return Math.floor(Math.random() * (99)+1);
  }

  getAllnotes(): Ifacture[]{
    let results: Ifacture [] = [];
    this.storage.keys()
    .then(keys =>
      keys.filter(key =>key.includes(STORAGE_KEY))
      .forEach(key =>
      this.storage.get(key).then((data :Ifacture) => results.push(data))
    )
   );
   return results;
  
  }

  getEverything(){
    let results = [];
    this.storage.forEach( ele => results.push(JSON.parse(ele)));
    return results;
  }


/*   getNoteFraisDispo(note: Ifacture[]) {
    let exist = !this.notes_frais.getNoteDispo();
    if(!exist){
      this.notes_frais.saveNote(note);
      return false;
    }
    else{
      return exist
    }
  } */

  removeAllnotes(){
    this.storage.keys()
     .then(keys =>
      keys.filter(key =>key.includes(STORAGE_KEY))
      .forEach(key =>
        this.storage.remove(key).then( (data) => console.log('done' + data)))
      );
  }


 
  saveNote(note: Ifacture[]){
    //console.log(this.getNoteFraisDispo(note))
    console.log(note);

    this.storage.set(this.getNoteKey(), JSON.stringify(note));
  }
}
