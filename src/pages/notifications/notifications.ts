import {Component, Output, EventEmitter} from "@angular/core";
import {ViewController, NavController, NavParams} from "ionic-angular";
import { SaisiePage } from "../saisie/saisie";

@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html'
})

export class NotificationsPage {


  constructor(public viewCtrl: ViewController,
            public navCtrl: NavController,
   ) {
    
            }

  close() {
    this.viewCtrl.dismiss();
  }

  expensesSelection(){
  this.viewCtrl.dismiss(true)
}

  goToSaisie(){
      this.navCtrl.push(SaisiePage);
  }
}
