import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MynotesProvider } from '../../providers/mynotes/mynotes';
import { Ifacture } from '../../interface/Ifacture';
import { ApiProvider } from '../../providers/api/api';
import {Observable} from "rxjs/Observable";
import { Http} from '@angular/http';
import { HomePage } from '../home/home';
import { Database } from '../../providers/database/database';
import { Inote_frais, Status } from '../../interface/Inote_frais';
import { NotesfraisPage } from '../notesfrais/notesfrais';
import { DepensesPage } from '../depenses/depenses';



/**
 * Generated class for the DashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dash',
  templateUrl: 'dash.html',
})
export class DashPage {
  depenses: Ifacture[];
  restaurants: any;
  depense : Ifacture;
  note_frais : Inote_frais = {titre: 'notefrais', status: Status.verouille, montant: 45};
  notes: Inote_frais[];
  notes_results: Inote_frais;
  test: Inote_frais;

  private url: string = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=44.840024,%20%20-0.586275&radius=500&type=restaurant&key=%20AIzaSyB9IwISvShHQqIIeeczKWvZzWJXp2FVoFo'

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private http: Http,
              private mynotes: MynotesProvider,
              public api: ApiProvider,
              private dbprovider: Database) {
                this.dbprovider.initialiseDB();

  }

  loadDepenses(){

    this.dbprovider.getDepenses().then ((data) => {
      this.depenses = <Ifacture[]> data;
          console.log(this.depenses);

    },(error) =>
        console.log(error)
  )
}

deleteTables(){
  this.dbprovider.deleteTables();
}

goToDetailNote(note: Inote_frais){
  this.dbprovider.getDepensesFromNote(note).then((notes: Ifacture[]) =>{
    console.log(notes);
    this.depenses = <Ifacture[]>notes;
    console.log(this.depenses);
  this.navCtrl.push(NotesfraisPage, this.depenses);
  })
}


  loadNotes(){
     this.dbprovider.getNotesFrais().then ((data) => {
      this.notes = <Inote_frais[]> data;
          console.log(this.notes);

    },(error) =>
        console.log(error)
  );  
}

goToDepenses(){
  this.navCtrl.push(DepensesPage);
}

checkNoteEnCours(){
  this.dbprovider.checkedNoteEnCours().then((data) => {
     
    this.notes_results = <Inote_frais> data;
    console.log(this.notes_results);
   })
}


  


  create() {
    this.dbprovider.addDepense(this.depense).then(data => {
      console.log('Note en cours :'+ data);
      if(data == undefined){
        this.dbprovider.addNoteFrais(this.depense)
              .then(data => { 
            console.log('Nouvelle note ajoutee', +JSON.stringify(data));
            this.dbprovider.addDepense(this.depense)
          });
        
      }else{
      this.dbprovider.updateNoteFrais(this.depense, data).then(data => console.log(data))
      }
    })  
  }

  deleteNote(){
  this.dbprovider.deleteAllNoteFrais();
  this.dbprovider.deleteAllDepenses();

  }

    
  

  getNote(){
  this.dbprovider.getNote('14').then(data => {
    console.log(data);
    console.log(typeof(data));
    this.test = data;
    console.log(this.test);
  })
  }



  createNote(){
    this.dbprovider.addNoteFrais(this.depense);
    }

  add(){
  }




















  getResto(){
    this.api.getResto()
    .subscribe(
      (resultArray) => {  
      this.restaurants = resultArray;
      console.log(this.restaurants);
      }
     // error => console.log("Error ::" + error)
    );
    
  }


}
