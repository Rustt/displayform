import { Component } from '@angular/core';
import { IonicPage, NavController,LoadingController, NavParams, Platform, ViewController, ActionSheetController, normalizeURL, AlertController, ToastController } from 'ionic-angular';
import { SaisiePage } from '../saisie/saisie';
import { DepensesPage } from '../depenses/depenses';
import { CategoriesPage } from '../categories/categories';
import { Database } from '../../providers/database/database';
import {Inote_frais, Status} from '../../interface/Inote_frais'
import { stringify } from '@angular/compiler/src/util';
import {Ifacture} from '../../interface/Ifacture';
import { NotesfraisPage } from '../notesfrais/notesfrais';
import {CameraProvider} from '../../providers/camera/camera';

import { ThemingProvider } from '../../providers/theming/theming';


/**
 * Generated class for the HomedisplayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-homedisplay',
  templateUrl: 'homedisplay.html',
})
export class HomedisplayPage {

  note_en_cours: Inote_frais;
  depenses: Ifacture[];
  nbr_depenses:number =0;
  montant: number = 0;
  changeimg:boolean;
  chosenPicture: any;
  images:any = [];



  selectedTheme: String;
  

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public cameraProvider: CameraProvider,
              public actionsheetCtrl: ActionSheetController,
              public loadingCtrl: LoadingController,
              public toast: ToastController,
              private db: Database,
            private plt: Platform,
            private alertCtrl: AlertController,
            private settings: ThemingProvider) {
              this.changeimg = true;
              this.settings.getActiveTheme().subscribe(val => this.selectedTheme = val);

              this.db.checkedNoteEnCours().then(data => {
                this.note_en_cours = <Inote_frais> data;
                //console.log(this.note_en_cours.montant);
                if(data!== null){
                  console.log(this.note_en_cours.montant);
                  this.montant = this.note_en_cours.montant;
                  this.db.getDepensesFromNote(this.note_en_cours).then((depenses) =>{
                    this.depenses = <Ifacture[]> depenses;
                    this.nbr_depenses = this.depenses.length;
                    console.log(this.depenses);
          
               });
              }else{
                this.montant = 0;
                this.nbr_depenses = 0;
              }
                });

      

  }
/* 
  ionViewDidLoad() {
    console.log('ionViewDidLoad HomedisplayPage');
     this.db.checkedNoteEnCours().then(data => {
      this.note_en_cours = <Inote_frais> data;
      console.log(this.note_en_cours.montant);
      if(data!== null){
        console.log(this.note_en_cours.montant);
        this.montant = this.note_en_cours.montant;
        this.db.getDepensesFromNote(this.note_en_cours).then((depenses) =>{
          this.depenses = <Ifacture[]> depenses;
          this.nbr_depenses = this.depenses.length;
          console.log(this.depenses);
        });
    }
    });
  }
 */
  ionViewWillEnter(){
      this.db.checkedNoteEnCours().then(data => {
      this.note_en_cours = <Inote_frais> data;
      //console.log(this.note_en_cours.montant);
      if(data!== null){
        console.log(this.note_en_cours.montant);
        this.montant = this.note_en_cours.montant;
        this.db.getDepensesFromNote(this.note_en_cours).then((depenses) =>{
          this.depenses = <Ifacture[]> depenses;
          if (this.depenses == undefined){
            this.montant = 0;
            this.nbr_depenses = 0;
          }else{
          this.nbr_depenses = this.depenses.length;
          console.log(this.depenses);
          }

     });
    }else{
      this.montant = 0;
      this.nbr_depenses = 0;
    }
      });
    }
    
      
  



  goToDetailEnCours(){
  this.navCtrl.push(NotesfraisPage, this.note_en_cours);
  }


  goToSaisie(){
    this.navCtrl.push(SaisiePage);

  }

  goToDepenses(){
    this.navCtrl.push(DepensesPage, this.note_en_cours);
  }

  showOptions(event){
    
  }


  toggleAppTheme() {
    if (this.selectedTheme === 'theme1') {
      this.changeimg = true;
      this.settings.setActiveTheme('theme2');
      console.log('change');
    } else {
      this.changeimg = false;

      this.settings.setActiveTheme('theme1');
    }
  }



  changePicture() {

    const actionsheet = this.actionsheetCtrl.create({
      title: '',
      buttons: [
        {
          text: 'appareil photo',
          icon: !this.plt.is('ios') ? 'camera' : null,
          handler: () => {
            this.takePicture();
          }
        },
        {
          text: !this.plt.is('ios') ? 'gallerie' : 'camera roll',
          icon: !this.plt.is('ios') ? 'image' : null,
          handler: () => {
            this.getPicture();
          }
        },
        {
          text: 'cancel',
          icon: !this.plt.is('ios') ? 'close' : null,
          role: 'destructive',
          handler: () => {
            console.log('annulation de l\'opération');
          }
        }
      ]
    });
    return actionsheet.present();
  }

  takePicture() {
    const loading = this.loadingCtrl.create();

    loading.present();
    return this.cameraProvider.getPictureFromCamera().then(picture => {
      if (picture) {
        this.chosenPicture = picture;
        console.log(this.chosenPicture);
        this.images.push(picture);
        //this.chosenPicture = normalizeURL(this.chosenPicture);
        console.log(this.chosenPicture);
        //this.navCtrl.push(SaisiePage,this.chosenPicture);
        this.alertImage();

      }
      loading.dismiss();
    }, error => {
      alert(error);
    });
  }

  getPicture() {
    const loading = this.loadingCtrl.create();

    loading.present();
    return this.cameraProvider.getPictureFromPhotoLibrary().then(picture => {
  
      if (picture) {

        this.chosenPicture = picture;
        console.log(this.chosenPicture);
        this.images.push(picture);
        //this.chosenPicture = normalizeURL(this.chosenPicture);
        console.log(this.chosenPicture);
        //this.navCtrl.push(SaisiePage,this.chosenPicture);
        this.alertImage();
      }
      loading.dismiss();
      
    });
  }


  alertImage(){
    let alert = this.alertCtrl.create({
      title:'Sauvegarde effectuée',
      //subTitle:'Montant TTC du justificatif',
      inputs: [
        {
          name: 'montant',
          type: 'tel',
          placeholder: 'Montant TTC'
        }
      ],
      message: 'Voulez vous compléter la dépense ?',
      buttons: [
       
        {
          text: 'Plus tard',
          handler: data => {
            let newnote: Ifacture = {};
            newnote.path = this.chosenPicture;
            newnote.prix = data.montant;
            console.log(data.montant)
            this.db.addDepense(newnote).then(data => {
              this.db.getIDcheckedNoteEnCours().then(id => {
              console.log(id);
             this.db.updateNoteFrais(newnote, id).then(() => {
              console.log('Note enregistrée');
              const toast = this.toast.create({
                message : 'Justificatif associé à la note en cours, pensez à compléter la dépense ultérieurement',
                position: "top",
                duration:3000
              });
              this.navCtrl.push(HomedisplayPage);

              //toast.onDidDismiss(this.dismissHandler);
              toast.present();

          })  
        });
          });
            
          }
        },
        {
          text: 'Maintenant',
          handler: data => {
            console.log('Go to saisie' + JSON.stringify(data));
            
            this.navCtrl.push(SaisiePage, {prix: data.montant , path: this.chosenPicture});
          }
        },
      ]
    });
    alert.present();
  }




  
}
