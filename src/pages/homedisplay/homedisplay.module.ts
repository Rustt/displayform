import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomedisplayPage } from './homedisplay';

@NgModule({
  declarations: [
    HomedisplayPage,
  ],
  imports: [
    IonicPageModule.forChild(HomedisplayPage),
  ],
})
export class HomedisplayPageModule {}
