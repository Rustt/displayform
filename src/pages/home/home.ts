import { Component } from '@angular/core';
import { Platform, NavController,  LoadingController, ToastController, NavParams } from 'ionic-angular';
import { FileOpener } from '@ionic-native/file-opener';
import {File} from '@ionic-native/file';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { MynotesProvider } from '../../providers/mynotes/mynotes';
import { PdfdocProvider } from '../../providers/pdfdoc/pdfdoc';
import { Ifacture } from '../../interface/Ifacture';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  savedParentNativeURLs = [];
  items: any;

  ownerTableBody = [
    [
        { text: 'Nom'.toUpperCase(), style: 'tableHeader' },
        { text: 'Prénom'.toUpperCase(), style: 'tableHeader' },
        { text: 'Email'.toUpperCase(), style: 'tableHeader' }
    ],
    [
        {text: 'Comet', style: 'tableCell'},
        {text: 'Martin', style: 'tableCell'},
        {text: 'myemail@email', style: 'tableCell'}
    ]
];

tabFrais = [
    [
        { text: 'Date'.toUpperCase(), style: 'tableHeader' },
        { text: 'Catégorie'.toUpperCase(), style: 'tableHeader' },
        { text: 'Montant TTC'.toUpperCase(), style: 'tableHeader' },
        { text: 'Montant HT'.toUpperCase(), style: 'tableHeader' },
        { text: 'Montant TVA'.toUpperCase(), style: 'tableHeader' }
    ]
];


  imageURI: any;
  imageFileName:any;
  
  noteForm: Ifacture;

  pdfObj = null;
  pathFile = null;

  constructor(public navCtrl: NavController, 
    private plt: Platform, 
    public navParams: NavParams,
    private mynotesprovider: MynotesProvider,
    private file: File, 
    private fileOpener: FileOpener,
    private transfer: FileTransfer,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public camera: Camera,
    public pdfdocprovider: PdfdocProvider){
    plt.ready()
      .then(() => {
      })
  }

   ionViewDidLoad(){
    document.addEventListener ('deviceready', onDeviceReady, false) ;
function onDeviceReady() {console.log(FileTransfer +'plugin ready');}

    this.noteForm = this.navParams.data;
    console.log(JSON.stringify(this.noteForm));
  } 

  createPdf(){
    console.log(JSON.stringify(this.noteForm));

    var docDefinition = {
      info: {
          title: 'Note de frais',
          author: 'JePilote Mes Factures'
      },
      pageSize: 'A4',
      pageMargins: [ 40, 40, 40, 20 ],
      content: [
          {
              text: 'Note de frais',
              style: 'title'
          },
          {
              style: 'table',
              table: {
                  headerRows: 1,
                  widths: [ '20%', '20%', '60%' ],
                  body: this.ownerTableBody
              },
              layout: 2
          },
          {
              table: {
                  headerRows: 1,
                  widths: [ '30%', '30%' ],
                  body: [
                      [{ text: 'DU', style: 'tableHeader' }, { text: 'AU', style: 'tableHeader' }],
                      //[{text: $scope.getDateJMAFromString($scope.periodeNdfDu), style: 'tableCell'}, {text: $scope.getDateJMAFromString($scope.periodeNdfAu), style: 'tableCell'}]
                  ]
              },
              layout: 2
          },
          {
              text: 'Tableau récapitulatif des frais',
              style: 'titleLeft'
          },
          {
              style: 'table',
              table: {
                  headerRows: 1,
                  widths: [ '20%', '35%', '15%', '15%', '15%' ],
                  body: this.tabFrais
              },
              layout: 1
          },
          {
              table: {
                  headerRows: 1,
                  widths: [ '20%', '20%', '20%' ],
                  body: [
                      [
                          { text: 'TOTAL TTC', style: 'tableHeader' },
                          { text: 'TOTAL HT', style: 'tableHeader' },
                          { text: 'TOTAL TVA', style: 'tableHeader' }
                      ] //,
             /*          [
                          {text: '€' + $scope.ndfTotal.ttc, style: 'tableCell'},
                          {text: '€' + $scope.ndfTotal.ht, style: 'tableCell'},
                          {text: '€' + $scope.ndfTotal.tva, style: 'tableCell'}
                      ] */
                  ]
              },
          }
      ] //,
    }
        this.pdfObj = pdfMake.createPdf(docDefinition);
    console.log('success');
  }


  downloadPdf(){
    if( this.plt.is('cordova')){
      this.pdfObj.getBuffer((buffer) => {
        var utf8 = new Uint8Array(buffer);
        var binaryArray = utf8.buffer;
        var blob = new Blob([binaryArray], {type: 'application/pdf'});

        this.file.writeFile(this.file.dataDirectory, this.noteForm.titre + '.pdf', blob, {replace: true}).then(fileEntry =>{
          this.fileOpener.open(this.file.dataDirectory + this.noteForm.titre + '.pdf', 'application/pdf');
          console.log(this.file.dataDirectory);
      })
    });

    }else {
      this.pdfObj.download();
    }
    this.pathFile = this.file.dataDirectory + 'myletter.pdf';
    console.log(this.pathFile);
  }

    getImage() {
      const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.FILE_URI,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
      }
    
      this.camera.getPicture(options).then((imageData) => {
        this.imageURI = imageData;
      }, (err) => {
        console.log(err);
        this.presentToast(err);
      });
    }
    
  

  uploadFile() {
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();
    const fileTransfer: FileTransferObject = this.transfer.create();
   
    let options: FileUploadOptions = {
      fileKey: 'ionicfile',
      fileName: 'ionicfile',
      chunkedMode: false,
      mimeType: "image/pdf",
      headers: {}
    } 
   // https://test-melodie.jepilote.com/banks/upload_file
    fileTransfer.upload(this.pathFile,'')
      .then((data) => {
      console.log(JSON.stringify(data) +" Uploaded Successfully");
      //this.imageFileName = "http://192.168.0.7:8080/static/images/ionicfile.jpg";
      loader.dismiss();
      this.presentToast("Image uploaded successfully");
    }, (err) => {
      console.log(err + 'Not uploaded');
      loader.dismiss();
      console.log(JSON.stringify(err));
      this.presentToast(JSON.stringify(err));
    });
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }



} //end class








/*

  listRootDir = () => {

    const ROOT_DIRECTORY = "file:///";

    (<any> window).resolveLocalFileSystemURL(ROOT_DIRECTORY,
      (fileSystem) => {

        var reader = fileSystem.createReader();
        reader.readEntries(
          (entries) => {
            this.ngZone.run(()=> {
              this.items = entries;
            });
          }, this.handleError);
      }, this.handleError);
}

  handleError = (error) => {
  console.log('error reading,', error)
  };

  goDown = (item) => {

    let childName = this.items[0].name;
    let childNativeURL = this.items[0].nativeURL;

    const parentNativeURL = childNativeURL.replace(childName, '');

    this.savedParentNativeURLs.push(parentNativeURL);

    var reader = item.createReader();

    reader.readEntries(
      (children) => {
        this.ngZone.run(()=> {
          this.items = children;
        })
      }, this.handleError);
  }



  goUp = () => {

    const parentNativeURL = this.savedParentNativeURLs.pop();

    (<any> window).resolveLocalFileSystemURL(parentNativeURL,
      (fileSystem) => {
      
        var reader = fileSystem.createReader();
      
        reader.readEntries(
          (entries) => {
            this.ngZone.run(()=> {
              this.items = entries;
            })
          }, this.handleError);
      }, this.handleError);
}



}
*/



