import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SansJustifPage } from './sans-justif';

@NgModule({
  declarations: [
    SansJustifPage,
  ],
  imports: [
    IonicPageModule.forChild(SansJustifPage),
  ],
})
export class SansJustifPageModule {}
