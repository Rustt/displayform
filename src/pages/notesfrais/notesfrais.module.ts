import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotesfraisPage } from './notesfrais';

@NgModule({
  declarations: [
    NotesfraisPage,
  ],
  imports: [
    IonicPageModule.forChild(NotesfraisPage),
  ],
})
export class NotesfraisPageModule {}
