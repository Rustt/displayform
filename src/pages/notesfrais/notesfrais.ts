import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Ifacture } from '../../interface/Ifacture';
import { Database } from '../../providers/database/database';
import { Inote_frais } from '../../interface/Inote_frais';
import { DepensesPage } from '../depenses/depenses';
import { SaisiePage } from '../saisie/saisie';

import { SuperTabsModule} from 'ionic2-super-tabs';


/**
 * Generated class for the NotesfraisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notesfrais',
  templateUrl: 'notesfrais.html',
})
export class NotesfraisPage {

  note_en_cours:Inote_frais;
  depenses:Ifacture[];
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private dbprovider: Database) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotesfraisPage');
  }

  ionViewWillEnter(){
    //this.depenses = this.navParams.data;
    this.note_en_cours = this.navParams.data;
    if(this.note_en_cours !== undefined){
    this.dbprovider.getDepensesFromNote(this.note_en_cours).then((depenses) =>{
      this.depenses = <Ifacture[]> depenses;
      console.log(this.depenses);
    });
  }

  }

  getNoteEnCours(){
    this.dbprovider.checkedNoteEnCours().then(data => {
      this.note_en_cours = <Inote_frais> data;
      console.log(this.note_en_cours.montant);
      this.dbprovider.getDepensesFromNote(this.note_en_cours).then((depenses) =>{
        this.depenses = <Ifacture[]> depenses;
        console.log(this.depenses);
      });
    });
  }

  goToDetailDepense(depense: Ifacture){
    this.navCtrl.push(SaisiePage,depense)
  }



}
