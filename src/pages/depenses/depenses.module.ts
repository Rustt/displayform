import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {BrowserModule} from '@angular/platform-browser'

import { DepensesPage } from './depenses';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';

@NgModule({
  declarations: [
    DepensesPage,
  ],
  imports: [
    IonicPageModule.forChild(DepensesPage),
    BrowserModule, FormsModule, ReactiveFormsModule,

  ],
})
export class DepensesPageModule {}
