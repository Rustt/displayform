import { Component, NgModule, Input, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, Platform, ViewController } from 'ionic-angular';
import { MynotesProvider } from '../../providers/mynotes/mynotes';
import { Ifacture } from '../../interface/Ifacture';


import { NotesfraisPage } from '../../pages/notesfrais/notesfrais';

import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NotificationsPage } from '../notifications/notifications';
import { NotesfraisProvider } from '../../providers/notesfrais/notesfrais';
import { Database } from '../../providers/database/database';
import { Inote_frais, Status } from '../../interface/Inote_frais';

//AFFIHAGE BUG  === npm install --save @ionic-native/sqlite pour actualiser plugin 

@IonicPage()
@Component({
  selector: 'page-depenses',
  templateUrl: 'depenses.html',
})
export class DepensesPage{
  
  path = "assets/imgs/hotel.png"
  checked: boolean = false;
  ischecked: boolean;
  myForm: FormGroup;
  newnote: Inote_frais;
  depenses: Ifacture[];
  merging:boolean;
  notes_de_frais: Inote_frais[];
  noteFormArray: FormArray;

  notes_results: Inote_frais;

  note_en_cours: Inote_frais;

  note_frais : Inote_frais = {titre: 'notefrais', status: Status.verouille, montant: 45};


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private fb: FormBuilder,
    public popoverCtrl: PopoverController,
    private dbprovider: Database,
    private plt: Platform) {

      this.plt.ready().then(() => {
      this.dbprovider.initialiseDB(); 
      })
  }

  ngOnInit() {
    this.myForm =this.fb.group({
      newnote: this.fb.array([])
    });
  }

  getNoteEnCours(){
    this.dbprovider.checkedNoteEnCours().then(data => {
      this.note_en_cours = <Inote_frais> data;
      console.log(this.note_en_cours);
      this.dbprovider.getDepensesFromNote(this.note_en_cours).then((depenses) =>{
        this.depenses = <Ifacture[]> depenses;
        console.log(this.depenses);
      });
    });
  }

  ionViewWillEnter(){
    //this.notes_de_frais = this.notesprovider.notes ;
    if(this.plt.ready()){
      this.dbprovider.getNotesFrais().then ((data) => {
        this.notes_de_frais = <Inote_frais[]> data;
            console.log(this.notes_de_frais);

      },(error) =>
          console.log(error)
      );
      //this.note_en_cours = this.navParams.data;
      this.getNoteEnCours();
      console.log(this.note_en_cours);
    }else{
      console.log('error');
    }
}

/*   loadNotes(){ //sauf note en cours
     this.dbprovider.getNotesFrais().then ((data) => {
      this.notes_de_frais = <Inote_frais[]> data;
          console.log(this.notes_de_frais);

    },(error) =>
        console.log(error)
  );  
} */



  loadDepenses(){

    this.dbprovider.getDepenses().then ((data) => {
      this.depenses = <Ifacture[]> data;
          console.log(this.depenses);

    },(error) =>
        console.log(error)
  )
}



goToDetailNote(note: Inote_frais){

  this.navCtrl.push(NotesfraisPage, note);
 
}






  


/*   create() {
    this.dbprovider.addDepense(this.depense).then(data => {
      console.log('Note en cours :'+JSON.stringify(data))
      this.dbprovider.updateNoteFrais(this.depense, data).then(note => console.log(data))
    },(error) =>
    console.log(error)
    )  
  } */

  deleteNote(){
  //this.dbprovider.deleteNoteFrais(13);
    this.dbprovider.deleteAllDepenses();

  }

    
  

  getNote(){
  this.dbprovider.getNote('14').then(data => {
    console.log(data);
    console.log(typeof(data));

  })
  }

  deleteDepenses(){
    this.dbprovider.deleteAllNoteFrais();
    }

/*   createNote(){
    this.dbprovider.addNoteFrais();
    }
 */



    sort(){

    }


  showOptions(ev){
  console.log(this.merging);
    let popover = this.popoverCtrl.create(NotificationsPage);
    popover.present({
      ev: ev
    });

    popover.onDidDismiss(data => {
      console.log(data);
      if(data!=null){
        this.merging = data;
      }
    })
  }
// rassembler dépenses
/*
  isChecked(note) {
    return this.myForm.controls.newnote.value.includes(note);
  }



  onChange(note: Ifacture, isChecked: boolean){
      this.noteFormArray = <FormArray>this.myForm.controls.newnote;
    console.log(this.noteFormArray);

    if(isChecked) {
      
      this.noteFormArray.push(new FormControl(note));

    } else {
      let index = this.noteFormArray.controls.findIndex(x => x.value == note)
      this.noteFormArray.removeAt(index);
    }
    console.log(this.myForm.value);
  
  }
  
  cancelNote() {
    this.merging =false;
  }

  goToDetail(note: Ifacture){
    
  }
 // fusion dépenses
makeNote(){
 this.newnote = this.myForm.get("newnote").value;
 this.notefrais.saveNote(this.newnote)
console.log(this.newnote);
if(this.noteFormArray){
  while (this.noteFormArray.length) {
 this.noteFormArray.removeAt(0);
  }
  this.newnote=null;
}
this.cancelNote();

}



deleteNote(frais : Inote_frais){
  this.notefrais.removeAllnotes()
  this.navCtrl.push(DepensesPage);

} */

}
