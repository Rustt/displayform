import { Component, ViewChild } from '@angular/core';


import { IonicPage, LoadingController, Platform, ActionSheetController, NavController, NavParams, Slides } from 'ionic-angular';
import {CameraProvider} from '../../providers/camera/camera';
import { HomePage } from '../home/home';
import { DashPage } from '../dash/dash';
import { DepensesPage } from '../depenses/depenses';
import { Ifacture } from '../../interface/Ifacture';
import { MynotesProvider } from '../../providers/mynotes/mynotes';
import { NgForm } from '@angular/forms';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import {FileEntry} from '@ionic-native/file';
import {ExpandableComponent} from '../../components/expandable/expandable';
import { HomedisplayPage } from '../homedisplay/homedisplay';
import { Database } from '../../providers/database/database';
/**
 * Generated class for the SaisiePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-saisie',
  templateUrl: 'saisie.html',
})
export class SaisiePage {
  chosenPicture: any = '';
  fact:Ifacture;
  images:any = [];
  titrenote:string;
  montant:string= '0';
  depense: Ifacture;
  chosenCategorie: {titre: string, path: string};

  categories = [{titre: 'peage', path:'assets/imgs/peage.png', selected:false},
                {titre: 'restaurant', path:'assets/imgs/restau.png', selected:false},
                {titre: 'hotel', path: 'assets/imgs/hotel.png', selected:false}
  ];
  datetoday: String = new Date().toISOString();

  @ViewChild('slider') slider: Slides;

  depenseForm = new FormGroup({
    titre: new FormControl('',
              [Validators.required]),
    prix: new FormControl(this.montant,
              [Validators.required,
              Validators.pattern(/^[1-9]+[0-9]*$/)
            ]),
    date: new FormControl(this.datetoday,
                      [Validators.required])
    
  });

/*   form = new FormGroup({
    fieldname: new FormControl(
                   initial value, 
                   synchronous validators, 
                   asynchronous validators)
  }); */



  placeholder ='assets/imgs/logo.png';

  constructor(public navCtrl: NavController,
               public navParams: NavParams,
               public actionsheetCtrl: ActionSheetController,
               public cameraProvider: CameraProvider,
               public platform: Platform,
               private mynotes: MynotesProvider,
               public loadingCtrl: LoadingController,
              private dbprovider: Database) {
                this.fact = <Ifacture>this.navParams.data;
                console.log(this.fact);
                console.log(this.fact.date);
                if(this.navParams.get('prix') !== undefined){
      this.depenseForm.setValue({prix: this.fact.prix, titre:this.fact.titre, date:this.fact.date});
      this.chosenPicture = this.fact.path;
    
      
                }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SaisiePage');
  }

  get titre(){
    return this.depenseForm.get("titre");
}
  ionViewWillEnter(){

  }


  // ne pas enregistrer une nouvelle note si on vient notedefraisPage
  logForm(){
  
     // console.log(this.todo);
      console.log(this.depenseForm.value);

  /*      console.log(JSON.parse(JSON.stringify(this.todo)));
      console.log(this.todo.titre); */
     this.fact = <Ifacture> this.depenseForm.value;
     /* this.fact.categorie = this.chosenCategorie.path;
    this.fact.path = this.chosenPicture; */
    
    if(this.chosenPicture !== undefined){
          this.fact.path = this.chosenPicture;

    }
    if (this.chosenCategorie.path !==undefined){
          this.fact.categorie = this.chosenCategorie.path;
          console.log(this.fact.categorie);

    }
    console.log(this.fact);
    console.log(JSON.stringify(this.fact)); 
    //console.log(this.chosenCategorie.path);


    //push
    //this.todo.path = this.chosenPicture;
    //this.mynotes.saveNote(this.todo);


      this.dbprovider.addDepense(this.fact).then(data => {
      console.log('Note en cours :'+ data);
      if(data == undefined){
        this.dbprovider.addNoteFrais(this.fact)
              .then(data => { 
            console.log('Nouvelle note ajoutee', +JSON.stringify(data));
            this.dbprovider.addDepense(this.fact).then(() => this.navCtrl.push(HomedisplayPage))
          });
        
      }else{
      this.dbprovider.updateNoteFrais(this.fact, data).then(data => {
        console.log(data);
        this.navCtrl.push(HomedisplayPage);
      });
      }
    }) 
       

  
}

  toggleButton(){

  }

  goToDash(){
    this.navCtrl.push(DashPage);
  }

  onSlideChanged() {
    const currentIndex = this.slider.getActiveIndex();
    console.log('Slide changed! Current index is', currentIndex);
  }

  goToSlide(categorie:{titre: string, path: string, selected: boolean}) {

    const currentIndex = this.slider.getActiveIndex();
  this.slider.slideTo(currentIndex, 500);
  console.log('Slide changed! Current index is', currentIndex);
    console.log("catégorie" + categorie.titre)


    for(let i=0; i<this.categories.length;i++){
      if(this.categories[i].selected === true){
        this.categories[i].selected =false;
      }
    categorie.selected = true;
    this.chosenCategorie = categorie;
    }
    
    

      }

  addImage(){
    this.getPicture();
  }

  changePicture() {

    const actionsheet = this.actionsheetCtrl.create({
      title: '',
      buttons: [
        {
          text: 'appareil photo',
          icon: !this.platform.is('ios') ? 'camera' : null,
          handler: () => {
            this.takePicture();
          }
        },
        {
          text: !this.platform.is('ios') ? 'gallerie' : 'camera roll',
          icon: !this.platform.is('ios') ? 'image' : null,
          handler: () => {
            this.getPicture();
          }
        },
        {
          text: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          role: 'destructive',
          handler: () => {
            console.log('annulation de l\'opération');
          }
        }
      ]
    });
    return actionsheet.present();
  }

  takePicture() {
    const loading = this.loadingCtrl.create();

    loading.present();
    return this.cameraProvider.getPictureFromCamera().then(picture => {
      if (picture) {
        this.chosenPicture = picture;
      }
      loading.dismiss();
    }, error => {
      alert(error);
    });
  }

  getPicture() {
    const loading = this.loadingCtrl.create();

    loading.present();
    return this.cameraProvider.getPictureFromPhotoLibrary().then(picture => {
  
      if (picture) {

        this.chosenPicture = picture;
        this.images.push(picture);
        console.log(this.images);
      }
      loading.dismiss();

    });
  }
}
