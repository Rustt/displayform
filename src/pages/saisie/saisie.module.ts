import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SaisiePage } from './saisie';
import { ComponentsModule } from "../../components/components.module";
@NgModule({
  declarations: [
    SaisiePage,
  ],
  imports: [
    IonicPageModule.forChild(SaisiePage),
    ComponentsModule
  ],
  exports:[
    SaisiePage,
    
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
],
})
export class SaisiePageModule {}
