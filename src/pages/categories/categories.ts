import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SaisiePage } from '../saisie/saisie';


/**
 * Generated class for the CategoriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
})
export class CategoriesPage {

 
  images: Array<{title: string, path: string}> =[
    {title: "Hôtel", path: "assets/imgs/hotel.png"},
    {title: "Restaurant", path: "assets/imgs/restau.png"},
    {title: "Péage", path: "assets/imgs/peage.png"},

  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goToSaisie(image: {title: string, path: string}){//passer le path en paramètre
    this.navCtrl.push(SaisiePage);
  }

}
