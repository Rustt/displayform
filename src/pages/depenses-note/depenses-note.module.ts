import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DepensesNotePage } from './depenses-note';

@NgModule({
  declarations: [
    DepensesNotePage,
  ],
  imports: [
    IonicPageModule.forChild(DepensesNotePage),
  ],
})
export class DepensesNotePageModule {}
