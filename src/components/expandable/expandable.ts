import { Component, ViewChild, OnInit, Renderer, Input, ElementRef } from '@angular/core';

/**
 * Generated class for the AccordionComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'expandable',
  templateUrl: 'expandable.html'
})
export class ExpandableComponent{

  accordionExapanded: boolean;
  @ViewChild("cc") cardContent: ElementRef;

  icon: string = "ios-arrow-down";

  viewHeight: number;
  constructor(public renderer: Renderer) {

  }

  ngAfterViewInit() {
    console.log(this.cardContent.nativeElement);
    this.viewHeight = this.cardContent.nativeElement.offsetHeight;

   // this.renderer.setElementStyle(this.cardContent.nativeElement, "webkitTransition", "max-height 500ms, padding 500ms");
  this.accordionExapanded = false;
  }

  toggleAccordion() {
    if (this.accordionExapanded) {
      this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "0px");
      this.renderer.setElementStyle(this.cardContent.nativeElement, "padding", "0px 0px");

    } else {
      this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "500px");
      this.renderer.setElementStyle(this.cardContent.nativeElement, "padding", "0px 0px");

    }

    this.accordionExapanded = !this.accordionExapanded;
    this.icon = this.icon == "ios-arrow-down" ? "ios-arrow-forward" : "ios-arrow-down";

  }

}