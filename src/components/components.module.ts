import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ExpandableComponent } from './expandable/expandable';
import { IonicModule } from 'ionic-angular';
import { BrowserModule } from '@angular/platform-browser';
import { MyApp } from '../app/app.component';
@NgModule({
	declarations: [ExpandableComponent],
	imports: [IonicModule],
	exports: [ExpandableComponent],
	schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ComponentsModule {}
