import { NgModule, ErrorHandler, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler, ToastController } from 'ionic-angular';
import { MyApp } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicStorageModule } from "@ionic/storage";
import { ImageCropperModule } from "ng2-img-cropper/index";

import { Base64ToGallery } from '@ionic-native/base64-to-gallery';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { SaisiePage } from '../pages/saisie/saisie';
import {DashPage} from '../pages/dash/dash';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MynotesProvider } from '../providers/mynotes/mynotes';

import {File} from '@ionic-native/file';
import {FileOpener} from '@ionic-native/file-opener';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { PdfdocProvider } from '../providers/pdfdoc/pdfdoc';
//import { PhotoProvider } from '../providers/photo/photo';
import { Crop } from '@ionic-native/crop';
import { CameraProvider } from '../providers/camera/camera';
import { DepensesPage } from '../pages/depenses/depenses';
import { ApiProvider } from '../providers/api/api';
import { HttpClientModule, HttpClient} from '@angular/common/http';
import { Http, HttpModule } from '@angular/http';
import { ExpandableComponent } from '../components/expandable/expandable';
import {NotificationsPage} from '../pages/notifications/notifications';
import {SQLite} from '@ionic-native/sqlite';
import { Database } from '../providers/database/database';
import { NotesfraisProvider } from '../providers/notesfrais/notesfrais';
import { NotesfraisPage} from '../pages/notesfrais/notesfrais';
import { SuperTabsModule} from 'ionic2-super-tabs';
import {HomedisplayPage} from '../pages/homedisplay/homedisplay'; 
import  { CategoriesPage } from '../pages/categories/categories';
import { ThemingProvider } from '../providers/theming/theming';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    SaisiePage,
    DashPage,
    DepensesPage,
    ExpandableComponent,
    NotificationsPage,
    NotesfraisPage,
    HomedisplayPage,
    CategoriesPage,
    TabsPage,
    
    
    
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    ImageCropperModule,
    HttpClientModule,
    HttpModule,
    SuperTabsModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    SaisiePage,
    DashPage,
    DepensesPage,
    NotificationsPage,
    NotesfraisPage,
    HomedisplayPage,
    CategoriesPage,
    TabsPage
    
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    MynotesProvider,
    File,
    FileOpener,
    FileTransfer,
    FileTransferObject,
    Camera,
    PdfdocProvider,
    Base64ToGallery,
    Crop,
    CameraProvider,
    ApiProvider,
    HttpClient,
    SQLite,
    Database,
    NotesfraisProvider,
    ToastController,
    ThemingProvider
    
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
],
})

export class AppModule {}
