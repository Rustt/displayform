import { Component, NgModule } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { AboutPage } from '../pages/about/about';
import { SaisiePage } from '../pages/saisie/saisie';
import { DashPage } from '../pages/dash/dash';
import {DepensesPage} from '../pages/depenses/depenses';
import {DepensesNotePage} from '../pages/depenses-note/depenses-note';

import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { HomedisplayPage } from '../pages/homedisplay/homedisplay';
import {timer} from 'rxjs/observable/timer';
import { ThemingProvider } from '../providers/theming/theming';
import { NotesfraisPage } from '../pages/notesfrais/notesfrais';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomedisplayPage;
  showSplash = true;
  state: any;
  selectedTheme: String;




  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private settings: ThemingProvider) {
    this.settings.getActiveTheme().subscribe(val => this.selectedTheme = val);
    platform.ready().then(() => {

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      timer(3000).subscribe(() => this.showSplash = false);
    });
  }
}
